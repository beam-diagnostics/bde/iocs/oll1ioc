importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.java.lang);

// create thread to update countdown at 1 s rate

// pv[0] is local trigger PV to start the thread
// pv[1] is $(P)$(R)Reload request PV
runnable = {
    run:function()
        {
            widget.setPropertyValue("enabled", false);
            var e = 3;
            while (e--) {
                Thread.sleep(1000);
                widget.setPropertyValue("text", ".. in "+e+" s..");
                ConsoleUtil.writeInfo(".. in "+e+" seconds..");
                if (! display.isActive()) {
                    ConsoleUtil.writeInfo("reload thread: display inactive, exiting thread ..");
                    return;
                }
            }
            ConsoleUtil.writeInfo("reload thread: loop done, exiting thread ..");
            // make sure we can reload again after IOC is back
            pvs[0].setValue(1);
            widget.setPropertyValue("text", "Reloading!");
            Thread.sleep(500);
            widget.setPropertyValue("text", "Reload");
            widget.setPropertyValue("enabled", true);
        }
    };

var s = PVUtil.getLong(pvs[0]);
var r = PVUtil.getLong(pvs[1]);
ConsoleUtil.writeInfo("reload thread: s="+s+" , r="+r+" ..");

// make sure we do not start the thread more than once and
// only if user requested reload
if (s == 1 && r == 1) {
    pvs[0].setValue(0);
    ConsoleUtil.writeInfo("reload thread: starting");
    new Thread(new Runnable(runnable)).start();
} else {
    ConsoleUtil.writeInfo("reload thread: NOT starting");
}
