importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

//Avoid running this script if the script is triggered during opi startup.
var flagName = "firstRun";
if (widgetController.getExternalObject(flagName) == null) {
    widgetController.setExternalObject(flagName, false);
} else {
    var macroInput = DataUtil.createMacrosInput(true);
    var p = PVUtil.getString(pvs[0]);
    if (p == 1) {
        pvs[0].setValue(0);
        var loc = PVUtil.getString(pvs[1]);
        ConsoleUtil.writeInfo("new location '"+loc+"'");

        //Create a new Macro Input
        var macroInput = DataUtil.createMacrosInput(true);
        //Put a macro in the new Macro Input
        macroInput.put("LOC", loc);
        macroInput.put("P", "OLL-"+loc+":");
        macroInput.put("R", "");

        //Set the macro input of the linking container to this new macro input.
        widgetController.setPropertyValue("macros", macroInput);

        //Reload the OPI file in the linking container again
        //by setting the property value with forcing fire option in true.
        widgetController.setPropertyValue("opi_file",
            widgetController.getPropertyValue("opi_file"), true);

        ConsoleUtil.writeInfo("OPI reloaded!");
    }
}
