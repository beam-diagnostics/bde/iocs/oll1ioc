importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

//Avoid running this script if the script is triggered during opi startup.
var flagName = "firstRun";
if (widgetController.getExternalObject(flagName) == null) {
    widgetController.setExternalObject(flagName, false);
} else {
    var d = PVUtil.getLong(pvs[2]);
    if (d == 1) {
        var macroInput = DataUtil.createMacrosInput(true);
        var m = PVUtil.getString(pvs[0]);
        // ConsoleUtil.writeInfo("item name '"+m+"'");
        var p = PVUtil.getString(pvs[1]);
        // ConsoleUtil.writeInfo("opi path '"+p+"'");
        // replace $(R) macro
        var r = m+":";
        macroInput.put("R", r);
        // add $(R0) macro, to retain the unit $(R) for child OPI
        macroInput.put("R0", r);

        var opi = p;
        ConsoleUtil.writeInfo("open OPI '"+opi+"'");
        ScriptUtil.openOPI(widgetController, opi, 0, macroInput);

        pvs[2].setValue(0);
    }
}
