importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
importPackage(Packages.java.lang);

// create thread to update date time at .5 s rate

// pv[0] is local trigger PV to start the thread
runnable = {
    run:function()
        {
            while (1) {
                Thread.sleep(500);
                dt = new Date().toString();
                widget.setPropertyValue("text", dt);
                //ConsoleUtil.writeInfo("now : "+dt);
                if (! display.isActive()) {
                    ConsoleUtil.writeInfo("datetime thread: display inactive, exiting thread ..");
                    return;
                }
            }
        }
    };

var s = PVUtil.getLong(pvs[0]);
ConsoleUtil.writeInfo("datetime thread: s="+s+" ..");

// make sure we do not start the thread more than once!
if (s == 1) {
    pvs[0].setValue(0);
    ConsoleUtil.writeInfo("datetime thread: starting");
    new Thread(new Runnable(runnable)).start();
} else {
    ConsoleUtil.writeInfo("datetime thread: NOT starting");
}
