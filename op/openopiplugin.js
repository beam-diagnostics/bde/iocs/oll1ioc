importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

//Avoid running this script if the script is triggered during opi startup.
var flagName = "firstRun";
if (widgetController.getExternalObject(flagName) == null) {
    widgetController.setExternalObject(flagName, false);
} else {
    var d = PVUtil.getLong(pvs[0]);
    if (d == 1) {
        var macroInput = DataUtil.createMacrosInput(true);
        var p = PVUtil.getString(pvs[1]);
        // ConsoleUtil.writeInfo("plugin type '"+p+"'");
        // replace $(R) macro
        var r ="$(R)$(S)";
        var opiMacro = widget.getMacroValue("OPI");
        // ConsoleUtil.writeInfo("OPI macro '"+opiMacro+"'");
        macroInput.put("R", r);
        // ConsoleUtil.writeInfo("R="+r);
        // construct opi filename
        var n = p;
        // get rid of the spaces in the plugin type name
        var i = p.indexOf(" ");
        if (i != -1) {
            n = p.slice(0, i);
        }
        // determine the plugin OPI filename by looking at the plugin type
        // or using supplied OPI filename from OPI macro 
        if (opiMacro != "") {
        	n = opiMacro;
        } else if (n.indexOf("NDPlugin") != -1) {
            n = "ND"+n.slice(8);
        }
        var opi ="common/"+n+".opi";
        // ConsoleUtil.writeInfo("open OPI '"+opi+"'");
        ScriptUtil.openOPI(widgetController, opi, 4, macroInput);

        pvs[0].setValue(0);
    }
}
