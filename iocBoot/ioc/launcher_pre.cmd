###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
#
#  START of launcher module

# PREFIX is set in st.cmd

epicsEnvSet("PORT",                     "OLL")
epicsEnvSet("EPICS_DB_INCLUDE_PATH",    "$(EPICS_DB_INCLUDE_PATH):$(LAUNCHER)/db")
# MAX_ITEMS: maximum number of items to support
epicsEnvSet("MAX_ITEMS",                "10")

# Always create a launcher driver
# launcherConfigure(const char *portName, int maxItems)
launcherConfigure("$(PORT)", $(MAX_ITEMS))
# top level controls
dbLoadRecords("launcher.template",  "P=$(PREFIX),R=,   PORT=$(PORT),ADDR=0, TIMEOUT=1")
# individual item controls
# set the NAME macro the name of the startup file (without _pre.cmd/_post.cmd)
dbLoadRecords("launcherN.template", "P=$(PREFIX),R=0:, PORT=$(PORT),ADDR=0, TIMEOUT=1, NAME=ITEM0, OPI=ITEM")
dbLoadRecords("launcherN.template", "P=$(PREFIX),R=1:, PORT=$(PORT),ADDR=1, TIMEOUT=1, NAME=ITEM1, OPI=ITEM")
dbLoadRecords("launcherN.template", "P=$(PREFIX),R=2:, PORT=$(PORT),ADDR=2, TIMEOUT=1, NAME=ITEM2, OPI=ITEM")
dbLoadRecords("launcherN.template", "P=$(PREFIX),R=3:, PORT=$(PORT),ADDR=3, TIMEOUT=1, NAME=ITEM3, OPI=ITEM")
dbLoadRecords("launcherN.template", "P=$(PREFIX),R=4:, PORT=$(PORT),ADDR=4, TIMEOUT=1, NAME=ITEM4, OPI=ITEM")
dbLoadRecords("launcherN.template", "P=$(PREFIX),R=5:, PORT=$(PORT),ADDR=5, TIMEOUT=1, NAME=ITEM5, OPI=ITEM")
dbLoadRecords("launcherN.template", "P=$(PREFIX),R=6:, PORT=$(PORT),ADDR=6, TIMEOUT=1, NAME=ITEM6, OPI=ITEM")
dbLoadRecords("launcherN.template", "P=$(PREFIX),R=7:, PORT=$(PORT),ADDR=7, TIMEOUT=1, NAME=ITEM7, OPI=ITEM")
dbLoadRecords("launcherN.template", "P=$(PREFIX),R=8:, PORT=$(PORT),ADDR=8, TIMEOUT=1, NAME=ITEM8, OPI=ITEM")
dbLoadRecords("launcherN.template", "P=$(PREFIX),R=9:, PORT=$(PORT),ADDR=9, TIMEOUT=1, NAME=ITEM9, OPI=ITEM")

set_requestfile_path("$(LAUNCHER)/req")
set_pass0_restoreFile("oll_settings.sav")
set_pass1_restoreFile("oll_settings.sav")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

# END of launcher module
###############################################################################
