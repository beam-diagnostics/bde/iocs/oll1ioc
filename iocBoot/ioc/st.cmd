###############################################################################
#
#  Optics Lab Launcher
#
###############################################################################

< envPaths

# LOC: location of electronics (rack row, lab, dev,..), to distinguish
# possible multiple instances of this IOC
epicsEnvSet("LOC",                      "DEV")
# PREFIX: prefix for all records; $(P) macro in db files
epicsEnvSet("PREFIX",                   "OLL-$(LOC):")

< main.cmd
