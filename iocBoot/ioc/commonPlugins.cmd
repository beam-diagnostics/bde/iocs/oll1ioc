# This is an example file for creating plugins
# It uses the following environment variable macros
# Many of the parameters defined in this file are also in commonPlugins_settings.req so if autosave is being
# use the autosave value will replace the value passed to this file.

# $(PREFIX)      Prefix for all records
# $(PORT)        The port name for the detector.  In autosave.
# $(QSIZE)       The queue size for all plugins.  In autosave.
# $(XSIZE)       The maximum image width; used to set the maximum size for row profiles in the NDPluginStats plugin and 1-D FFT
#                   profiles in NDPluginFFT.
# $(YSIZE)       The maximum image height; used to set the maximum size for column profiles in the NDPluginStats plugin
# $(NCHANS)      The maximum number of time series points in the NDPluginStats, NDPluginROIStats, and NDPluginAttribute plugins
# $(CBUFFS)      The maximum number of frames buffered in the NDPluginCircularBuff plugin
# $(MAX_THREADS) The maximum number of threads for plugins which can run in multiple threads. Defaults to 5.

# Create a TIFF file saving plugin
NDFileTIFFConfigure("$(PORT)-TIFF1", $(QSIZE), 0, "$(PORT)", 0)
dbLoadRecords("NDFileTIFF.template",  "P=$(PREFIX),R=$(PORT):TIFF1:,PORT=$(PORT)-TIFF1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")

# Create an HDF5 file saving plugin
NDFileHDF5Configure("$(PORT)-HDF1", $(QSIZE), 0, "$(PORT)", 0)
dbLoadRecords("NDFileHDF5.template",  "P=$(PREFIX),R=$(PORT):HDF1:,PORT=$(PORT)-HDF1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")

# Create 4 ROI plugins
NDROIConfigure("$(PORT)-ROI1", $(QSIZE), 0, "$(PORT)", 0, 0, 0, 0, 0, $(MAX_THREADS=5))
dbLoadRecords("NDROI.template",       "P=$(PREFIX),R=$(PORT):ROI1:,  PORT=$(PORT)-ROI1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")
NDROIConfigure("$(PORT)-ROI2", $(QSIZE), 0, "$(PORT)", 0, 0, 0, 0, 0, $(MAX_THREADS=5))
dbLoadRecords("NDROI.template",       "P=$(PREFIX),R=$(PORT):ROI2:,  PORT=$(PORT)-ROI2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")
NDROIConfigure("$(PORT)-ROI3", $(QSIZE), 0, "$(PORT)", 0, 0, 0, 0, 0, $(MAX_THREADS=5))
dbLoadRecords("NDROI.template",       "P=$(PREFIX),R=$(PORT):ROI3:,  PORT=$(PORT)-ROI3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")
NDROIConfigure("$(PORT)-ROI4", $(QSIZE), 0, "$(PORT)", 0, 0, 0, 0, 0, $(MAX_THREADS=5))
dbLoadRecords("NDROI.template",       "P=$(PREFIX),R=$(PORT):ROI4:,  PORT=$(PORT)-ROI4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")

# Create 4 processing plugins
NDProcessConfigure("$(PORT)-PROC1", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("NDProcess.template",   "P=$(PREFIX),R=$(PORT):PROC1:,  PORT=$(PORT)-PROC1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")
NDProcessConfigure("$(PORT)-PROC2", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("NDProcess.template",   "P=$(PREFIX),R=$(PORT):PROC2:,  PORT=$(PORT)-PROC2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")
NDProcessConfigure("$(PORT)-PROC3", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("NDProcess.template",   "P=$(PREFIX),R=$(PORT):PROC3:,  PORT=$(PORT)-PROC3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")
NDProcessConfigure("$(PORT)-PROC4", $(QSIZE), 0, "$(PORT)", 0, 0, 0)
dbLoadRecords("NDProcess.template",   "P=$(PREFIX),R=$(PORT):PROC4:,  PORT=$(PORT)-PROC4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")

# Create 5 statistics plugins
NDStatsConfigure("$(PORT)-STAT1", $(QSIZE), 0, "$(PORT)", 0, 0, 0, 0, 0, $(MAX_THREADS=5))
dbLoadRecords("NDStats.template",     "P=$(PREFIX),R=$(PORT):STAT1:,  PORT=$(PORT)-STAT1,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),NDARRAY_PORT=$(PORT)")
NDTimeSeriesConfigure("$(PORT)-STAT1_TS", $(QSIZE), 0, "$(PORT)-STAT1", 1, 23)
dbLoadRecords("NDTimeSeries.template",  "P=$(PREFIX),R=$(PORT):STAT1:TS:, PORT=$(PORT)-STAT1_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)-STAT1,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")
NDStatsConfigure("$(PORT)-STAT2", $(QSIZE), 0, "$(PORT)-ROI1",    0, 0, 0, 0, 0, $(MAX_THREADS=5))
dbLoadRecords("NDStats.template",     "P=$(PREFIX),R=$(PORT):STAT2:,  PORT=$(PORT)-STAT2,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),NDARRAY_PORT=$(PORT)")
NDTimeSeriesConfigure("$(PORT)-STAT2_TS", $(QSIZE), 0, "$(PORT)-STAT2", 1, 23)
dbLoadRecords("NDTimeSeries.template",  "P=$(PREFIX),R=$(PORT):STAT2:TS:, PORT=$(PORT)-STAT2_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)-STAT2,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")
NDStatsConfigure("$(PORT)-STAT3", $(QSIZE), 0, "$(PORT)-ROI2",    0, 0, 0, 0, 0, $(MAX_THREADS=5))
dbLoadRecords("NDStats.template",     "P=$(PREFIX),R=$(PORT):STAT3:,  PORT=$(PORT)-STAT3,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),NDARRAY_PORT=$(PORT)")
NDTimeSeriesConfigure("$(PORT)-STAT3_TS", $(QSIZE), 0, "$(PORT)-STAT3", 1, 23)
dbLoadRecords("NDTimeSeries.template",  "P=$(PREFIX),R=$(PORT):STAT3:TS:, PORT=$(PORT)-STAT3_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)-STAT3,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")
NDStatsConfigure("$(PORT)-STAT4", $(QSIZE), 0, "$(PORT)-ROI3",    0, 0, 0, 0, 0, $(MAX_THREADS=5))
dbLoadRecords("NDStats.template",     "P=$(PREFIX),R=$(PORT):STAT4:,  PORT=$(PORT)-STAT4,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),NDARRAY_PORT=$(PORT)")
NDTimeSeriesConfigure("$(PORT)-STAT4_TS", $(QSIZE), 0, "$(PORT)-STAT4", 1, 23)
dbLoadRecords("NDTimeSeries.template",  "P=$(PREFIX),R=$(PORT):STAT4:TS:, PORT=$(PORT)-STAT4_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)-STAT4,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")
NDStatsConfigure("$(PORT)-STAT5", $(QSIZE), 0, "$(PORT)-ROI4",    0, 0, 0, 0, 0, $(MAX_THREADS=5))
dbLoadRecords("NDStats.template",     "P=$(PREFIX),R=$(PORT):STAT5:,  PORT=$(PORT)-STAT5,ADDR=0,TIMEOUT=1,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),NDARRAY_PORT=$(PORT)")
NDTimeSeriesConfigure("$(PORT)-STAT5_TS", $(QSIZE), 0, "$(PORT)-STAT5", 1, 23)
dbLoadRecords("NDTimeSeries.template",  "P=$(PREFIX),R=$(PORT):STAT5:TS:, PORT=$(PORT)-STAT5_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)-STAT5,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")

# Create a fits plugin
NDFitsConfigure("$(PORT)-FIT1", $(QSIZE), 0, "$(PORT)", 0, 1)
dbLoadRecords("NDFits.template",  "P=$(PREFIX),R=$(PORT):FIT1:,  PORT=$(PORT)-FIT1,ADDR=0,TIMEOUT=1,XSIZE=$(XSIZE),NDARRAY_PORT=$(PORT)")
dbLoadRecords("NDFitsN.template", "P=$(PREFIX),R=$(PORT):FIT1:1:,PORT=$(PORT)-FIT1,ADDR=0,TIMEOUT=1")
