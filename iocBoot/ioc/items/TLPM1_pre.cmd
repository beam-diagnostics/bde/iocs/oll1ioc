###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
#
#  START of tlpm100 module

# PREFIX are set in st.cmd
epicsEnvSet("PORT",                     "TLPM1")
epicsEnvSet("TLPM1R",                   "TLPM1:")
epicsEnvSet("EPICS_DB_INCLUDE_PATH",    "$(EPICS_DB_INCLUDE_PATH):$(TLPM100)/db")
epicsEnvSet("STREAM_PROTOCOL_PATH",     "$(STREAM_PROTOCOL_PATH):$(TLPM100)/proto")

# uncomment to enable lots of debug on stream
# var streamDebug 1

# Create an asynUSBTMC driver
# usbtmcConfigure(port, vendorNum, productNum, serialNumberStr, priority, flags)
usbtmcConfigure("$(PORT)")
dbLoadRecords("tlpm100.template",    "P=$(PREFIX),R=$(PORT):,PORT=$(PORT)")
dbLoadRecords("asynRecord.db",       "P=$(PREFIX),R=asyn,    PORT=$(PORT),ADDR=0,OMAX=100,IMAX=100")

# Create a standard arrays plugin, set it to get data from first simDetector driver.
# NDStdArraysConfigure("$(PORT)-DAT1", 20, 0, "$(PORT)", 0, 0, 0, 0, 0, 5)
# Make NELEMENTS in the following be a little bigger than 2048*2048
# dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=$(PORT):DAT1:,PORT=$(PORT)-DAT1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=SHORT,NELEMENTS=4200000")

# < commonPlugins.cmd
set_requestfile_path("$(TLPM100)/req")
set_pass0_restoreFile("$(PORT)_settings.sav")
set_pass1_restoreFile("$(PORT)_settings.sav")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

# END of tlpm100 module
###############################################################################
