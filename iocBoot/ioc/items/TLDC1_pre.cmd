###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
#
#  START of tldc3100 module

# PREFIX are set in st.cmd
epicsEnvSet("PORT",                     "TLDC1")
epicsEnvSet("TLDC1R",                   "TLDC1:")
epicsEnvSet("EPICS_DB_INCLUDE_PATH",    "$(EPICS_DB_INCLUDE_PATH):$(TLDC3100)/db")

# uncomment to enable lots of debug on stream
# var streamDebug 1

# Create a tlDC3100 driver
# tlDC3100Config(const char *portName, int usbVID, int usbPID,
#                const char *serialNumber, int priority, int stackSize)
tlDC3100Config("$(PREFIX)", 0x1313, 0x8060, "M00417186")
dbLoadRecords("tldc3100.template",   "P=$(PREFIX),R=$(PORT):,PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("asynRecord.db",       "P=$(PREFIX),R=asyn,    PORT=$(PORT),ADDR=0,OMAX=100,IMAX=100")

# Create a standard arrays plugin, set it to get data from first simDetector driver.
# NDStdArraysConfigure("$(PORT)-DAT1", 20, 0, "$(PORT)", 0, 0, 0, 0, 0, 5)
# Make NELEMENTS in the following be a little bigger than 2048*2048
# dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=$(PORT):DAT1:,PORT=$(PORT)-DAT1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=SHORT,NELEMENTS=4200000")

# < commonPlugins.cmd
set_requestfile_path("$(TLDC3100)/req")
set_pass0_restoreFile("$(PORT)_settings.sav")
set_pass1_restoreFile("$(PORT)_settings.sav")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

# END of tldc3100 module
###############################################################################
