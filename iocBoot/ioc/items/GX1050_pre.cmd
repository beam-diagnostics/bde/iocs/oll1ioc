#
# AD simDetector driver
#

# PREFIX is set in st.cmd

epicsEnvSet("PORT",                     "GX1050")
epicsEnvSet("XSIZE",                    "1024")
epicsEnvSet("YSIZE",                    "1024")
epicsEnvSet("EPICS_DB_INCLUDE_PATH",    "$(EPICS_DB_INCLUDE_PATH):$(ADARAVIS)/db")

aravisCameraConfig("$(PORT)", "Photonic Science-V3")
dbLoadRecords("aravisCamera.template", "P=$(PREFIX),R=$(PORT):,PORT=$(PORT),ADDR=0,TIMEOUT=1")
dbLoadRecords("PSL_FDI3.template","P=$(PREFIX),R=$(PORT):,PORT=$(PORT),ADDR=0,TIMEOUT=1")

# NDStdArraysConfigure("Image1", 20, 0, "$(PORT)", 0, 0, 0, 0, 0, 5)
# dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=SHORT,NELEMENTS=12000000")

# Load all other plugins using commonPlugins.cmd which is the same for all AD drivers that are
# included in the oll ioc
< commonPlugins.cmd
set_requestfile_path("$(ADARAVIS)/req")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

##############################################################################
