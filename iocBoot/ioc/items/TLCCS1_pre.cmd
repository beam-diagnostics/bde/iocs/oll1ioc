###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
#
#  START of tlccs module

# PREFIX are set in st.cmd
# QSIZE, CBUFFs, .. are set in main.cmd
# RSCSTR: resource string for locating USB device: USB::VID::PID::SERIAL::RAW
#epicsEnvSet("RSCSTR",                   "USB::0x1313::0x8089::M00414547::RAW")
epicsEnvSet("RSCSTR",                   "USB::0x1313::0x8089::M00462436::RAW")
epicsEnvSet("PORT",                     "TLCCS1")
epicsEnvSet("TLCCS1R",                  "TLCCS1:")
epicsEnvSet("XSIZE",                    "3648")
epicsEnvSet("YSIZE",                    "1")
epicsEnvSet("EPICS_DB_INCLUDE_PATH",    "$(EPICS_DB_INCLUDE_PATH):$(ADTLCCS)/db")

# Create a TlCCS driver
# tlCCSConfig(const char *portName, int maxBuffers, size_t maxMemory,
#             const char *resourceName, int priority, int stackSize)
tlCCSConfig("$(PORT)", 0, 0, "$(RSCSTR)")
dbLoadRecords("tlccs.template",       "P=$(PREFIX),R=$(PORT):,PORT=$(PORT),ADDR=0,TIMEOUT=1,NELEMENTS=$(XSIZE)")

# Create a standard arrays plugin, set it to get data from first simDetector driver.
NDStdArraysConfigure("$(PORT)-DAT1", 20, 0, "$(PORT)", 0, 0, 0, 0, 0, 5)
# Make NELEMENTS in the following be a little bigger than 3648
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=$(PORT):DAT1:,PORT=$(PORT)-DAT1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=4000")

< commonPlugins.cmd
set_requestfile_path("$(ADTLCCS)/req")
set_pass0_restoreFile("$(PORT)_settings.sav")
set_pass1_restoreFile("$(PORT)_settings.sav")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

# END of tlccs module
###############################################################################
