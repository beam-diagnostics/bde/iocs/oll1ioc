###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
#
#  START of simdetector module

# PREFIX are set in st.cmd
# QSIZE, CBUFFs, .. are set in main.cmd
epicsEnvSet("PORT",                     "SIM1")
epicsEnvSet("SIM1R",                    "SIM1:")
epicsEnvSet("XSIZE",                    "1024")
epicsEnvSet("YSIZE",                    "1024")
epicsEnvSet("EPICS_DB_INCLUDE_PATH",    "$(EPICS_DB_INCLUDE_PATH):$(ADSIMDETECTOR)/db")

# Create a simDetector driver
# simDetectorConfig(const char *portName, int maxSizeX, int maxSizeY, int dataType,
#                   int maxBuffers, int maxMemory, int priority, int stackSize)
simDetectorConfig("$(PORT)", $(XSIZE), $(YSIZE), 1, 0, 0)
# To have the rate calculation use a non-zero smoothing factor use the following line
#dbLoadRecords("simDetector.template",     "P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1,RATE_SMOOTH=0.2")
dbLoadRecords("simDetector.template","P=$(PREFIX),R=$(PORT):,PORT=$(PORT),ADDR=0,TIMEOUT=1")

# Create a standard arrays plugin, set it to get data from first simDetector driver.
NDStdArraysConfigure("$(PORT)-DAT1", 20, 0, "$(PORT)", 0, 0, 0, 0, 0, 5)
# Make NELEMENTS in the following be a little bigger than 2048*2048
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=$(PORT):DAT1:,PORT=$(PORT)-DAT1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=SHORT,NELEMENTS=4200000")

< commonPlugins.cmd
set_requestfile_path("$(ADSIMDETECTOR)/req")
set_pass0_restoreFile("$(PORT)_settings.sav")
set_pass1_restoreFile("$(PORT)_settings.sav")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

# END of simdetector module
###############################################################################
