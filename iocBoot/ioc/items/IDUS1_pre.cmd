###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
#
#  START of andor module

# PREFIX are set in st.cmd
# QSIZE, CBUFFs, .. are set in main.cmd
epicsEnvSet("PORT",                     "IDUS1")
epicsEnvSet("IDUS1R",                   "IDUS1:")
epicsEnvSet("XSIZE",                    "1024")
epicsEnvSet("YSIZE",                    "1024")
epicsEnvSet("EPICS_DB_INCLUDE_PATH",    "$(EPICS_DB_INCLUDE_PATH):$(ADANDOR)/db")

# Fix Andor SDK issue with stale SHM memory region that is not removed after unclean
# app exit!
system "bash items/andor-shm-fix.sh"

# Create a andorCCD driver
# andorCCDConfig(const char *portName, const char *installPath, int shamrockID,
#                int maxBuffers, size_t maxMemory, int priority, int stackSize)
# we keep the firmware and ini files in the supportAndor folder
andorCCDConfig("$(PORT)", "$(ADANDOR)/etc/andor", 0, 0, 0, 0, 0)
dbLoadRecords("andorCCD.template",   "P=$(PREFIX),R=$(PORT):,PORT=$(PORT),ADDR=0,TIMEOUT=1")
# Comment out the following lines if there is no Shamrock spectrograph
#shamrockConfig(const char *portName, int shamrockId, const char *iniPath,
#               int priority, int stackSize)
# shamrockConfig("$(PORT)-SR1", 0, "", 0, 0)
# dbLoadRecords("shamrock.template",   "P=$(PREFIX),R=$(PORT):SR1:,PORT=$(PORT)-SR1,TIMEOUT=1,PIXELS=1024")

# Create a standard arrays plugin, set it to get data from first simDetector driver.
NDStdArraysConfigure("$(PORT)-DAT1", 20, 0, "$(PORT)", 0, 0, 0, 0, 0, 5)
# Make NELEMENTS in the following be a little bigger than 2048*2048
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=$(PORT):DAT1:,PORT=$(PORT)-DAT1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=SHORT,NELEMENTS=4200000")

< commonPlugins.cmd
set_requestfile_path("$(ADANDOR)/req")
set_pass0_restoreFile("$(PORT)_settings.sav")
set_pass1_restoreFile("$(PORT)_settings.sav")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

# END of andor module
###############################################################################
