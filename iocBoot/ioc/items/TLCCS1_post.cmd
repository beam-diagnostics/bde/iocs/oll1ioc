# save things every thirty seconds
# PREFIX is set in st.cmd
# TLCCS1R is set in TLCCS1_pre.cmd
create_monitor_set("TLCCS1_settings.req", 30, "P=$(PREFIX), R=$(TLCCS1R)")
