###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
#
#  START of tlfw102 module

# PREFIX are set in st.cmd
epicsEnvSet("PORT",                     "TLFW1")
epicsEnvSet("TLFW1R",                   "TLFW1:")
epicsEnvSet("EPICS_DB_INCLUDE_PATH",    "$(EPICS_DB_INCLUDE_PATH):$(TLFW102)/db")
epicsEnvSet("STREAM_PROTOCOL_PATH",     "$(STREAM_PROTOCOL_PATH):$(TLFW102)/proto")
# SERIAL_PORT: serial port is connected to
epicsEnvSet("SERIAL_PORT",              "/dev/ttyUSB0")

# uncomment to enable lots of debug on stream
# var streamDebug 1

# Create an asynSerialPort driver
# drvAsynSerialPortConfigure(port, ttyName, priority, noAutoConnect, noProcessEosIn)
drvAsynSerialPortConfigure("$(PREFIX)", "$(SERIAL_PORT)", 0, 0, 0)
asynSetOption("$(PREFIX)", 0, "baud",   "115200")
asynSetOption("$(PREFIX)", 0, "bits",   "8")
asynSetOption("$(PREFIX)", 0, "parity", "none")
asynSetOption("$(PREFIX)", 0, "stop",   "1")
asynSetOption("$(PREFIX)", 0, "clocal", "Y")
asynSetOption("$(PREFIX)", 0, "crtscts","N")

# Create an asyn USBTMC driver
# usbtmcConfigure(port, vendorNum, productNum, serialNumberStr, priority, flags)
usbtmcConfigure("$(PREFIX)")
dbLoadRecords("tlfw102.template",    "P=$(PREFIX),R=$(PORT):,PORT=$(PORT)")
dbLoadRecords("asynRecord.db",       "P=$(PREFIX),R=asyn,    PORT=$(PORT),ADDR=0,OMAX=100,IMAX=100")

# Create a standard arrays plugin, set it to get data from first simDetector driver.
# NDStdArraysConfigure("$(PORT)-DAT1", 20, 0, "$(PORT)", 0, 0, 0, 0, 0, 5)
# Make NELEMENTS in the following be a little bigger than 2048*2048
# dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=$(PORT):DAT1:,PORT=$(PORT)-DAT1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=SHORT,NELEMENTS=4200000")

# < commonPlugins.cmd
set_requestfile_path("$(TLFW102)/req")
set_pass0_restoreFile("$(PORT)_settings.sav")
set_pass1_restoreFile("$(PORT)_settings.sav")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

# END of tlfw102 module
###############################################################################
