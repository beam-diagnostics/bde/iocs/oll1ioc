# save things every thirty seconds
# PREFIX is set in st.cmd
# TLMFF1R is set in TLMFF1_pre.cmd
create_monitor_set("TLMFF1_settings.req", 30, "P=$(PREFIX), R=$(TLMFF1R)")
