#!/bin/bash

set -u
set -e
set -x

IFS='
'
for line in $(ipcs -mtp | grep ' ioc '); do
  pid=$(echo "$line" | grep ioc | awk '{print $3;}')
  shmid=$(echo "$line" | grep ioc | awk '{print $1;}')
  if [[ ! -d /proc/$pid ]]; then
    ipcrm -m $shmid
  fi
done
