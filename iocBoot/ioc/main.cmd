###############################################################################
########    this snippet needs to be included in top level st.cmd     #########

errlogInit(20000)

dbLoadDatabase("$(TOP)/dbd/ollApp.dbd")
ollApp_registerRecordDeviceDriver(pdbbase)

# PREFIX is set in st.cmd
# PORT is set in the module startup files
# QSIZE, NCHANS, CBUFFS, MAX_THREADS are for AD plugins
epicsEnvSet("QSIZE",                    "20")
epicsEnvSet("NCHANS",                   "2048")
epicsEnvSet("CBUFFS",                   "500")
epicsEnvSet("MAX_THREADS",              "8")
# EPICS_DB_INCLUDE_PATH: list module db folders, update in sub-modules
epicsEnvSet("EPICS_DB_INCLUDE_PATH",    "$(ASYN)/db:$(ADCORE)/db:$(ADMISC)/db:$(TOP)/db")
# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request, increase in sub-modules if needed
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")
# STREAM_PROTOCOL_PATH: streamdevice protocol file path, update in sub-modules
epicsEnvSet("STREAM_PROTOCOL_PATH",     ".")

set_requestfile_path("./")
set_requestfile_path("./items")
set_requestfile_path("$(CALC)/req")
set_requestfile_path("$(ADCORE)/req")
set_requestfile_path("$(ADMISC)/req")
set_savefile_path("./autosave")
# calls to set_requestfile_path() are made in module pre script
# calls to set_pass0_restoreFile() and set_pass1_restoreFile() are made in module pre script
save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("$(AUTOSAVE)/db/save_restoreStatus.db", "P=$(PREFIX)")
# always load launcher support
< launcher_pre.cmd
# load any enabled items
< items_pre.cmd

iocInit()

# calls to create_monitor_set() are made in module post script
< launcher_post.cmd
< items_post.cmd
